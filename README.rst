Karate club network
===================

The file ``karate.gml`` contains the network of friendships between
the 34 members of a karate club at a US university, as described by
Wayne Zachary in 1977.  If you use these data in your work, please
cite [Zachary1977]_.

.. [Zachary1977]
   Wayne W. Zachary. An Information Flow Model for Conflict and
   Fission in Small Groups. *Journal of Anthropological Research*,
   33(4):452--473, 1977.
